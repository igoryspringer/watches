<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">New currency</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/currency">List of currency</a></li>
                    <li class="breadcrumb-item active">New currency</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="<?=ADMIN;?>/currency/add" method="post" data-toggle="validator">
                        <div class="card-body">
                            <div class="form-group has-feedback">
                                <label for="title">Currency name</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="Name of currency" data-error="Please, enter currency name" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="code">Code</label>
                                <input type="text" name="code" class="form-control" id="code" placeholder="Code of currency" data-error="Please, enter code of currency" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="symbol_left">Left symbol</label>
                                <input type="text" name="symbol_left" class="form-control" id="symbol_left" placeholder="Left symbol of currency" data-error="Please, enter left symbol of currency">
                            </div>
                            <div class="form-group">
                                <label for="symbol_right">Right symbol</label>
                                <input type="text" name="symbol_right" class="form-control" id="symbol_right" placeholder="Right symbol of currency" data-error="Please, enter right symbol of currency">
                            </div>
                            <div class="form-group has-feedback">
                                <label for="value">Currency value</label>
                                <input type="text" name="value" class="form-control" id="value" placeholder="Currency value" data-error="Please, enter value of currency. Float only" pattern="^[0-9.]{1,}$" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="base">
                                    <input type="checkbox" name="base">
                                    Base currency</label>
                            </div>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->