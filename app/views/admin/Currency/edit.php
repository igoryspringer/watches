<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit currency <?=$currency->title;?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>/"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?= ADMIN ?>/currency">List of currency</a></li>
        <li class="active">Edit currency <?=$currency->title;?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="<?= ADMIN ?>/currency/edit" role="form" data-toggle="validator">
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <label for="title">Currency name</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Name of currency" data-error="Please, enter currency name" required value="<?= h($currency->title) ?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="code">Code</label>
                            <input type="text" name="code" class="form-control" id="code" placeholder="Code of currency" data-error="Please, enter code of currency" required value="<?= h($currency->code) ?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="symbol_left">Left symbol</label>
                            <input type="text" name="symbol_left" class="form-control" id="symbol_left" placeholder="Left symbol of currency" value="<?= h($currency->symbol_left) ?>">
                        </div>
                        <div class="form-group">
                            <label for="symbol_right">Right symbol</label>
                            <input type="text" name="symbol_right" class="form-control" id="symbol_right" placeholder="Right symbol of currency" value="<?= h($currency->symbol_right) ?>">
                        </div>
                        <div class="form-group has-feedback">
                            <label for="value">Currency value</label>
                            <input type="text" name="value" class="form-control" id="value" placeholder="Currency value" data-error="Please, enter value of currency. Float only" pattern="^[0-9.]{1,}$" required value="<?= $currency->value ?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>
                                <input name="base" type="checkbox"<?php if($currency->base) echo ' checked' ?>> Base currency
                            </label>
                        </div>
                    <div class="box-footer">
                        <input type="hidden" name="id" value="<?= $currency->id ?>">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>