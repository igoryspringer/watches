<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add new product</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/product">List of products</a></li>
                    <li class="breadcrumb-item active">New product</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="<?=ADMIN;?>/product/add" method="post" data-toggle="validator" id="add">
                        <div class="card-body">
                            <div class="form-group has-feedback">
                                <label for="title">Name of product</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="Name of product" value="<?php isset($_SESSION['form_data']['title']) ? h($_SESSION['form_data']['title']) : null; ?>" required data-error="Please, enter login">
                                <div class="help-block with-errors"></div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Parent category</label>
                                <?php new \app\widgets\menu\Menu([
                                    'tpl' => WWW.'/menu/select.php',
                                    'container' => 'select',
                                    'cache' => 0,
                                    'cacheKey' => 'admin_select',
                                    'class' => 'form-control',
                                    'attrs' => [
                                        'name' => 'category_id',
                                        'id' => 'category_id',
                                    ],
                                    'prepend' => '<option>Choice a category</option>',
                                ]); ?>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords</label>
                                <input type="text" name="keywords" class="form-control" id="keywords" placeholder="Keywords" value="<?php isset($_SESSION['form_data']['keywords']) ? h($_SESSION['form_data']['keywords']) : null; ?>">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?php isset($_SESSION['form_data']['description']) ? h($_SESSION['form_data']['description']) : null; ?>">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="price">Price</label>
                                <input type="text" name="price" class="form-control" id="price" placeholder="Price" pattern="^[0-9.]{1,}$" value="<?php isset($_SESSION['form_data']['price']) ? h($_SESSION['form_data']['price']) : null; ?>" required data-error="Please, enter price. Only float numbers allowed">
                                <div class="help-block with-errors"></div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="old_price">Old price</label>
                                <input type="text" name="old_price" class="form-control" id="old_price" placeholder="Old price" pattern="^[0-9.]{1,}$" value="<?php isset($_SESSION['form_data']['old_price']) ? h($_SESSION['form_data']['old_price']) : null; ?>" data-error="Only float numbers allowed">
                                <div class="help-block with-errors"></div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="content">Content</label>
                                <textarea name="content" id="editor1" cols="80" rows="10"><?php isset($_SESSION['form_data']['content']) ? $_SESSION['form_data']['content'] : null; ?></textarea>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="status" checked> Status
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="hit"> Hit
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="related">Related products</label>
                                <select name="related[]" class="form-control select2" id="related" multiple></select>
                            </div>
                            <?php //debug($_SESSION); unset($_SESSION['single']); unset($_SESSION['multi']);?>
                            <?php new \app\widgets\filter\Filter(null, WWW.'/filter/admin_filter_tpl.php'); ?>
                            <div class="form-group">
                                <div class="col-md-4 fl">
                                    <div class="card card-danger file-upload">
                                        <div class="card-header">
                                            <h3 class="card-title">Basic image</h3>
                                        </div>
                                        <div class="card-body">
                                            <div id="single" class="btn btn-success" data-url="product/add-image" data-name="single">Choice file</div>
                                            <p><small>Recommended sizes: 125*200</small></p>
                                            <div class="single"></div>
                                        </div>
                                       <div class="overlay">
                                            <i class="fas fa-2x fa-sync-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 fl">
                                    <div class="card card-primary file-upload">
                                        <div class="card-header">
                                            <h3 class="card-title">Images of gallery</h3>
                                        </div>
                                        <div class="card-body">
                                            <div id="multi" class="btn btn-success" data-url="product/add-image" data-name="multi">Choice file(s)</div>
                                            <p><small>Recommended sizes: 700*1000</small></p>
                                            <div class="multi"></div>
                                        </div>
                                        <div class="overlay">
                                            <i class="fas fa-2x fa-sync-alt"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                    <?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->