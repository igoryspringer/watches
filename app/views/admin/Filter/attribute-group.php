<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Filters groups</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item">Filters groups</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="table-responsive">
                        <div class="card-body">
                        <a href="<?=ADMIN;?>/filter/group-add" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i>Add group</a>
                            <table class="table card-tabs table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($attrs_group as $item): ?>
                            <tr>
                                <td><?=$item->title;?></td>
                                <td>
                                    <a href="<?=ADMIN;?>/filter/group-edit?id=<?=$item->id;?>" class="edit" title="Change"><i class="fa fa-fw fa-pen text-primary"></i></a>&nbsp; &nbsp; &nbsp;
                                    <a href="<?=ADMIN;?>/filter/group-delete?id=<?=$item->id;?>" class="delete" title="Remove"><i class="fa fa-fw fa-database text-danger"></i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->