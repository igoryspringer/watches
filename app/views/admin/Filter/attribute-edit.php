<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit filter <?=h($attr->value);?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/filter/attribute">Filters</a></li>
                    <li class="breadcrumb-item active">Edit filter <?=h($attr->value);?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="<?=ADMIN;?>/filter/attribute-edit" method="post" data-toggle="validator">
                        <div class="card-body">
                            <div class="form-group has-feedback">
                                <label for="value">Name</label>
                                <input type="text" name="value" class="form-control" id="value" placeholder="Name of filter" data-error="Please, enter filter name" required value="<?=h($attr->value);?>">
                                <div class="help-block with-errors"></div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Group</label>
                                <select name="attr_group_id" id="category_id" class="form-control">
                                    <?php foreach ($attrs_group as $item): ?>
                                        <option value="<?=$item->id;?>"<?php if ($item->id == $attr->attr_group_id) echo ' selected';?>><?=$item->title;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="id" value="<?=$attr->id;?>">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->