<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">New filter</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/filter/attribute">Filters</a></li>
                    <li class="breadcrumb-item active">New filter</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="<?=ADMIN;?>/filter/attribute-add" method="post" data-toggle="validator"  id="add">
                        <div class="card-body">
                            <div class="form-group has-feedback">
                                <label for="value">Name</label>
                                <input type="text" name="value" class="form-control" id="value" placeholder="Name of filter" data-error="Please, enter filter name" required>
                                <div class="help-block with-errors"></div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Group</label>
                                <select name="attr_group_id" id="category_id" class="form-control">
                                    <option>Choice a group</option>
                                    <?php foreach ($group as $item): ?>
                                        <option value="<?=$item->id;?>"><?=$item->title;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->