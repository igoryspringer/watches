<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Clear cache</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item">Clear cache</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table card-tabs table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Cache of categories</td>
                                    <td>Menu of categories on the site. Cached for 1 hour</td>
                                    <td>
                                        <a href="<?=ADMIN;?>/cache/delete?key=category" class="delete" title="Remove"><i class="fa fa-fw fa-database text-danger"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cache of filters</td>
                                    <td>Cache of filters and groups of filters. Cached for 1 hour</td>
                                    <td>
                                        <a href="<?=ADMIN;?>/cache/delete?key=filter" class="delete" title="Remove"><i class="fa fa-fw fa-database text-danger"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->