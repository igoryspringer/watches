<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    Order # <?=$order['id'];?>
                    <?php if ($order['status'] != '1'): ?>
                        <a href="<?=ADMIN;?>/order/edit?id=<?=$order['id'];?>&status=1" class="btn btn-success btn-xs" title="Save approve">Approve</a>
                    <?php else: ?>
                        <a href="<?=ADMIN;?>/order/edit?id=<?=$order['id'];?>&status=0" class="btn btn-default btn-xs" title="Change">Edit</a>
                    <?php endif;?>
                    <a href="<?=ADMIN;?>/order/delete?id=<?=$order['id'];?>" class="btn btn-danger btn-xs delete" title="Remove">Delete</a>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/order">List of order</a></li>
                    <li class="breadcrumb-item">Order # <?=$order['id'];?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <td>Number of order</td>
                                    <td><?=$order['id'];?></td>
                                </tr>
                                <tr>
                                    <td>Created at order</td>
                                    <td><?=$order['date'];?></td>
                                </tr>
                                <tr>
                                    <td>Updated at order</td>
                                    <td><?=$order['update_at'];?></td>
                                </tr>
                                <tr>
                                    <td>Number item in order</td>
                                    <td><?=count($order_products);?></td>
                                </tr>
                                <tr>
                                    <td>Sum of order</td>
                                    <td><?=$order['sum'];?> <?=$order['currency'];?></td>
                                </tr>
                                <tr>
                                    <td>Customer</td>
                                    <td><?=$order['name'];?></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <?//=$order['status'] ? 'Closed' : 'New';?>
                                        <?php
                                        if($order['status'] == '1'){
                                            echo 'Closed';
                                        }elseif($order['status'] == '2'){
                                            echo 'Paid';
                                        }else{
                                            echo 'New';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Note</td>
                                    <td><?=$order['note'];?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <h3>Order details</h3>
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-border table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $qty = 0; foreach ($order_products as $product): ?>
                                    <tr>
                                        <td><?=$product->id;?></td>
                                        <td><?=$product->title;?></td>
                                        <td><?=$product->qty; $qty += $product->qty?></td>
                                        <td><?=$product->price;?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr class="active">
                                    <td colspan="2">
                                        <b>Total:</b>
                                    </td>
                                    <td><b><?=$qty;?></b></td>
                                    <td><b><?=$order['sum'];?> <?=$order['currency'];?></b></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->