<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit user <?=$user->name;?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN;?>/user">List of users</a></li>
                    <li class="breadcrumb-item">Edit user <?=$user->name;?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="<?=ADMIN;?>/user/edit" method="post" data-toggle="validator">
                        <div class="card-body">
                            <div class="form-group has-feedback">
                                <label for="login">Login</label>
                                <input type="text" class="form-control" name="login" id="login" value="<?=h($user->login);?>" placeholder="Enter login" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                            </div>
                            <div class="form-group has-feedback">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" value="<?=h($user->name);?>" placeholder="Enter name" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" id="email" value="<?=h($user->email);?>" placeholder="Enter e-mail" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="address">Address</label>
                                <input type="address" class="form-control" name="address" id="address" value="<?=h($user->address);?>" placeholder="Enter address" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select name="role" id="role" class="form-control">
                                    <option value="user"<?php if($user->role == 'user') echo ' selected'; ?>>User</option>
                                    <option value="admin"<?php if($user->role == 'admin') echo ' selected'; ?>>Administrator</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="id" value="<?=$user->id;?>">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h3>Order(s) of the user</h3>
                        <?php if ($orders):?>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Status</th>
                                        <th>Sum</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($orders as $order): ?>
                                        <?php
                                        if($order['status'] == '1'){
                                            $class = 'bg-gradient-success';
                                            $text = 'Closed';
                                        }elseif($order['status'] == '2'){
                                            $class = 'bg-gradient-info';
                                            $text = 'Paid';
                                        }else{
                                            $class = '';
                                            $text = 'New';
                                        }
                                        //$class = $order['status'] ? 'success' : ''; ?>
                                        <tr class="<?=$class;?>">
                                            <td><?=$order['id'];?></td>
                                            <td><?=$order['status'] ? 'Closed' : 'New';?></td>
                                            <td><?=$order['sum'];?> <?=$order['currency'];?></td>
                                            <td><?=$order['date'];?></td>
                                            <td><?=$order['update_at'];?></td>
                                            <td>
                                                <a href="<?=ADMIN;?>/order/view?id=<?=$order['id'];?>" title="Show"><i class="fa fa-fw fa-eye"></i></a> &nbsp; &nbsp; &nbsp;
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php else:?>
                            <p class="text-danger">User don't order...till</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->