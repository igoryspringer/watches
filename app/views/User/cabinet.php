<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?= PATH ?>">Home</a></li>
                <li>Personal cabinet <?=$_SESSION['user']['name'];?></li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--prdt-starts-->
<div class="prdt">
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-12">
                <div class="product-one login">
                    <div class="register-top heading">
                        <h2>Personal cabinet <?=$_SESSION['user']['name'];?></h2>
                    </div>

                    <a href="user/edit" class="btn btn-default">Edit personal data</a>
                    <a href="user/orders" class="btn btn-default">History of orders</a>

                </div>
            </div>
        </div>
    </div>
</div>
<!--product-end-->