<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?= PATH ?>">Home</a></li>
                <li><a href="<?= PATH ?>">Personal cabinet</a></li>
                <li>Edit personal data <?=$_SESSION['user']['name'];?></li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--prdt-starts-->
<div class="prdt">
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-12">
                <div class="product-one login">
                    <div class="card">
                        <form action="user/edit" method="post" data-toggle="validator">
                            <div class="card-body">
                                <div class="form-group has-feedback">
                                    <label for="login">Login</label>
                                    <input type="text" class="form-control" name="login" id="login" value="<?=h($_SESSION['user']['login']);?>" placeholder="Enter login" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" value="<?=h($_SESSION['user']['name']);?>" placeholder="Enter name" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="email">E-mail</label>
                                    <input type="email" class="form-control" name="email" id="email" value="<?=h($_SESSION['user']['email']);?>" placeholder="Enter e-mail" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="address">Address</label>
                                    <input type="address" class="form-control" name="address" id="address" value="<?=h($_SESSION['user']['address']);?>" placeholder="Enter address" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--product-end-->