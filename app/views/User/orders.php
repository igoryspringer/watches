<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?=PATH;?>">Home</a></li>
                <li><a href="<?=PATH;?>/user/cabinet">Personal cabinet <?=$_SESSION['user']['name'];?></a></li>
                <li>History of orders</li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--prdt-starts-->
<div class="prdt">
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-12 prdt-left">
                <?php if($orders): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10%">ID</th>
                                <th style="width: 30%">Status</th>
                                <th style="width: 20%">Sum</th>
                                <th style="width: 20%">Create at</th>
                                <th style="width: 20%">Update at</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($orders as $order): ?>
                                <?php
                                if($order['status'] == '1'){
                                    $class = 'success';
                                    $text = 'Closed';
                                }elseif($order['status'] == '2'){
                                    $class = 'info';
                                    $text = 'Paid';
                                }else{
                                    $class = '';
                                    $text = 'New';
                                }
                                ?>
                                <tr class="<?=$class;?>">
                                    <td><?=$order->id;?></td>
                                    <td><?=$text;?></td>
                                    <td><?=$order->sum;?> <?=$order->currency;?></td>
                                    <td><?=$order->date;?></td>
                                    <td><?=$order->update_at;?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <p class="text-danger">You have not made orders yet</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--product-end-->