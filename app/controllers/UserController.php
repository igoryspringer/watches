<?php

namespace app\controllers;

use app\models\User;

class UserController extends AppController
{
    public function signupAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            $data = $_POST;
            $user->load($data);
            //debug($user); die;
            if (!$user->validate($data) || !$user->checkUnique()) {
                //debug($user->errors);
                $user->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user_id = $user->save('user')) {
                    $_SESSION['success'] = 'User is registered.';

                    //...sign in after sign up...
                    $_SESSION['user']['id'] = $user_id;
                    $_SESSION['user']['login'] = $data['login'];
                    $_SESSION['user']['name'] = $data['name'];
                    $_SESSION['user']['address'] = $data['address'];
                    $_SESSION['user']['email'] = $data['email'];
                    //...sign in after sign up...

                } else {
                    $_SESSION['error'] = 'Error!';
                }
            }
            redirect();
            //die;
        }
        $this->setMeta('Registration');
    }

    public function loginAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            if ($user->login()) {
                $_SESSION['success'] = 'Authorization was successful!';
                redirect('/user/cabinet');
            } else {
                $_SESSION['error'] = 'Login or password is incorrect!';
            }
            redirect();
        }
        $this->setMeta('Sign in');
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
        redirect(PATH);
    }

    public function cabinetAction()
    {
        if (!User::checkAuth()) redirect();
        $this->setMeta('Personal cabinet');
    }

    public function editAction()
    {
        if (!User::checkAuth()) redirect('user/login');
        if (!empty($_POST)) {
            $user = new \app\models\admin\User();
            $data = $_POST;
            $data['id'] = $_SESSION['user']['id'];
            $data['role'] = $_SESSION['user']['role'];
            $user->load($data);
            if (!$user->attributes['password']) {
                unset($user->attributes['password']);
            } else {
                $user->attributes['password'] = password_hash(!$user->attributes['password'], PASSWORD_DEFAULT);
            }
            if (!$user->validate($data) || !$user->checkUnique()) {
                $user->getErrors();
                redirect();
            }
            if ($user->update('user', $_SESSION['user']['id'])) {
                foreach ($user->attributes as $k => $v) {
                    if ($k != 'password') $_SESSION['user'][$k] = $v;
                }
                $_SESSION['success'] = 'Changes stored';
            }
            redirect();
        }
        $this->setMeta('Change personal data');
    }

    public function ordersAction()
    {
        if (!User::checkAuth()) redirect('user/login');
        $orders = \R::findAll('order', 'user_id = ?', [$_SESSION['user']['id']]);
        $this->setMeta('History of orders');
        $this->set(compact('orders'));
    }
}