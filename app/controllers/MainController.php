<?php

namespace app\controllers;

use ishop\Cache;

class MainController extends AppController
{
    //public $layout = 'test';

    public function indexAction()
    {
        $brands = \R::find('brand', 'LIMIT 3');
        $hits = \R::find('product', "hit = '1' AND status = '1' LIMIT 8");
        $canonical = PATH;
        //debug($brands);
        $this->setMeta('Homepage', 'Description', 'Keywords');
        $this->set(compact('brands', 'hits'));
    }
}