<?php //$parent = isset($category['childes']); ?>
<li>
    <a href="category/<?=$category['alias'];?>"><?=$category['title'];?></a>
    <?php if (isset($category['childes'])): ?>
        <ul>
            <?=$this->getMenuHtml($category['childes']);?>
        </ul>
    <?php endif; ?>
</li>