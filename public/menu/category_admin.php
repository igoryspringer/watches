<?php
$parent = isset($category['childes']);
if (!$parent) {
    $delete = '<a href="'.ADMIN.'/category/delete?id='.$id.'" class="delete" title="Remove"><i class="fa fa-fw fa-database text-danger"></i></a>';
} else {
    $delete = '<i class="fa fa-fw fa-database text-blue bg-gray-active color-palette"></i>';
}
?>
<p class="item-p">
    <a href="<?=ADMIN;?>/category/edit?id=<?=$id;?>" class="list-group-item"><?=$category['title'];?></a>
    <span><?=$delete;?></span>
</p>
<?php if ($parent): ?>
    <div class="list-group">
        <?=$this->getMenuHtml($category['childes']);?>
    </div>
<?php endif; ?>